import 'primo-explore-eth-libraryh3lp-chat';
var app = angular.module('viewCustom', ['ethLibraryh3lpChatModule']);

app.component('prmExploreMainAfter',  {
        controller: 'ethLibraryh3lpChatController',
        template: `<eth-libraryh3lp-chat-component></eth-libraryh3lp-chat-component>`
    })

app.constant('ethLibraryh3lpChatConfig', {
    "url": "https://eu.libraryh3lp.com/chat/myqueue@chat.eu.libraryh3lp.com?skin=123"
});
