/**
* @ngdoc module
* @name ethLibraryh3lpChatModule
*
* @description
*
* adds chat button and chat box for LibraryH3lp
*
* <b>AngularJS Dependencies</b><br>
*
* <b>CSS/Image Dependencies</b><br>
* CSS eth-libraryh3lp-chat.css
*
*/
import {ethLibraryh3lpChatController} from './eth-libraryh3lp-chat.controller';
import {ethLibraryh3lpChatHtml} from './eth-libraryh3lp-chat.html';

export const ethLibraryh3lpChatModule = angular
    .module('ethLibraryh3lpChatModule', [])
        .filter('safeUrl', ['$sce', function ($sce) {
          return function (url) {
            if (/^https:\/\/(.+\.)?libraryh3lp\.com.+$/.test(url)) {
              return $sce.trustAsResourceUrl(url);
            }
          };
        }])
        .controller('ethLibraryh3lpChatController', ethLibraryh3lpChatController)
        .component('ethLibraryh3lpChatComponent',{
            controller: 'ethLibraryh3lpChatController',
            template: ethLibraryh3lpChatHtml
        })
