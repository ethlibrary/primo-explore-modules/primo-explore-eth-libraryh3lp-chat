# primo-explore-eth-libraryh3lp-chat

## Description

This Module adds a chat button and chat box for LibraryH3lp.

### Screenshot

#### Chat Box
![screenshot](https://gitlab.com/ethlibrary/primo-explore-modules/primo-explore-eth-libraryh3lp-chat/-/raw/master/screenshot1.jpg)


## Installation

1. Assuming you've installed and are using [primo-explore-devenv](https://github.com/ExLibrisGroup/primo-explore-devenv).

2. Navigate to your view root directory. For example:
    ```
    cd primo-explore/custom/MY_VIEW_ID
    ```
3. If you do not already have a package.json file in this directory, create one:
    ```
    npm init -y
    ```
4. Install this package:
    ```
    npm install primo-explore-eth-libraryh3lp-chat --save-dev
    ```

## Usage

Once installed, inject `ethLibraryh3lpChatModule` as a dependency, and then add the eth-libraryh3lp-chat-component directive to the prmExploreMainAfter component.

```js

import 'primo-explore-eth-libraryh3lp-chat';
var app = angular.module('viewCustom', ['ethLibraryh3lpChatModule']);

app.component('prmExploreMainAfter',  {
        controller: 'ethLibraryh3lpChatController',
        template: `<eth-libraryh3lp-chat-component></eth-libraryh3lp-chat-component>`
    })

```
### Config

You'll need to configure the module by passing your libraryh3lp url as an angular `constant`:

```js

app.constant('ethLibraryh3lpChatConfig', {
    "url": "https://eu.libraryh3lp.com/chat/<queue>@chat.eu.libraryh3lp.com?skin=<skin>"
});

```
